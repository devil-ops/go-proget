package proget

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/rs/zerolog/log"
	"moul.io/http2curl"
)

const (
	baseURL = "https://oneget.oit.duke.edu"
)

type Client struct {
	client    *http.Client
	UserAgent string
	Config    ClientConfig
	BaseURL   string
	Asset     AssetService
	// VRF       VRFService
	// Device    DeviceService
	// IP        IPService
	// Location  LocationService
	// Volume    VolumeService
}

type ClientConfig struct {
	Token string `json:"token,omitempty"`
}

type Response struct {
	*http.Response
}

type ErrorResponse struct {
	Message string `json:"errors"`
}

func (c *Client) sendRequest(req *http.Request, v interface{}) (*Response, error) {
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "application/json; charset=utf-8")
	req.Header.Add("X-ApiKey", c.Config.Token)
	// req.Header.Set("x-apikey", fmt.Sprintf("accesskey=%v; secretkey=%v", c.Config.AccessKey, c.Config.SecretKey))

	if os.Getenv("PRINT_CURL") != "" {
		command, _ := http2curl.GetCurlCommand(req)
		log.Debug().Str("command", command.String()).Msg("Curl command")
	}

	res, err := c.client.Do(req)
	req.Close = true
	if err != nil {
		log.Warn().Err(err).Msg("Error submitting request")
		return nil, err
	}
	// b, _ := ioutil.ReadAll(res.Body)

	defer res.Body.Close()

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		var errRes ErrorResponse
		b, _ := ioutil.ReadAll(res.Body)
		log.Debug().Str("response", string(b)).Msg("Response")
		if err = json.NewDecoder(res.Body).Decode(&errRes); err == nil {
			return nil, errors.New(errRes.Message)
		}

		if res.StatusCode == http.StatusTooManyRequests {
			return nil, fmt.Errorf("too many requests.  Check rate limit and make sure the userAgent is set right")
		} else if res.StatusCode == http.StatusNotFound {
			return nil, fmt.Errorf("that entry was not found, are you sure it exists?")
		} else {
			return nil, fmt.Errorf("error, status code: %d", res.StatusCode)
		}
	}

	b, _ := ioutil.ReadAll(res.Body)
	if string(b) != "" {
		if err = json.NewDecoder(bytes.NewReader(b)).Decode(&v); err != nil {
			return nil, err
		}
	} else {
		// When there is no body
		return nil, nil
	}
	r := &Response{res}

	return r, nil
}

func NewClient(config ClientConfig, httpClient *http.Client) *Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}

	userAgent := "go-proget"
	c := &Client{
		Config:    config,
		client:    httpClient,
		UserAgent: userAgent,
		BaseURL:   baseURL,
	}
	// c.getToken(&config)

	c.Asset = &AssetServiceOp{client: c}

	// c.Location = &LocationServiceOp{client: c}
	// c.Volume = &VolumeServiceOp{client: c}
	return c
}

func NewClientWithEnv(config *ClientConfig, httpClient *http.Client) (*Client, error) {
	if config == nil {
		config = &ClientConfig{}
	}
	token := os.Getenv("PROGET_KEY")
	if token == "" {
		return nil, errors.New("Must set PROGET_KEY")
	}
	config.Token = token
	return NewClient(*config, httpClient), nil
}
