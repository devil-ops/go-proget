package proget

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPathFromAssetListOpts(t *testing.T) {
	tests := map[string]struct {
		opts    AssetListOpts
		want    string
		wantErr bool
	}{
		"missing-name": {
			opts:    AssetListOpts{},
			want:    "",
			wantErr: true,
		},
		"plain-directory": {
			opts: AssetListOpts{
				Name: "foo",
			},
			want: "endpoints/foo/dir/?recursive=false",
		},
		"plain-opts": {
			opts: AssetListOpts{
				Name:      "foo",
				Directory: "bar",
			},
			want: "endpoints/foo/dir/bar?recursive=false",
		},
	}

	for _, tt := range tests {
		got, err := pathFromAssetListOpts(tt.opts)
		if tt.wantErr {
			require.Error(t, err)
			require.Equal(t, "", got)
		} else {
			require.NoError(t, err)
			require.Equal(t, tt.want, got)
		}

	}
}

func TestListAssets(t *testing.T) {
	expected, err := ioutil.ReadFile("./testdata/asset/list.json")
	if err != nil {
		panic(err)
	}
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, string(expected))
	}))
	defer svr.Close()
	client := NewClient(ClientConfig{}, nil)
	client.BaseURL = svr.URL

	u, _, err := client.Asset.List(nil, &AssetListOpts{
		Name: "foo",
	})
	require.NoError(t, err)
	require.IsType(t, []Asset{}, u)
	require.NotNil(t, u)
	require.Equal(t, 2, len(u))
}

func TestPathFromAssetDeleteOpts(t *testing.T) {
	tests := map[string]struct {
		opts    AssetDeleteOpts
		want    string
		wantErr bool
	}{
		"missing-name": {
			opts:    AssetDeleteOpts{},
			want:    "",
			wantErr: true,
		},
		"plain-directory": {
			opts: AssetDeleteOpts{
				Name: "foo",
			},
			want: "endpoints/foo/delete/?recursive=false",
		},
		"plain-opts": {
			opts: AssetDeleteOpts{
				Name: "foo",
				Path: "bar",
			},
			want: "endpoints/foo/delete/bar?recursive=false",
		},
	}

	for _, tt := range tests {
		got, err := pathFromAssetDeleteOpts(tt.opts)
		if tt.wantErr {
			require.Error(t, err)
			require.Equal(t, "", got)
		} else {
			require.NoError(t, err)
			require.Equal(t, tt.want, got)
		}

	}
}

func TestDeleteAssets(t *testing.T) {
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, string(``))
	}))
	defer svr.Close()
	client := NewClient(ClientConfig{}, nil)
	client.BaseURL = svr.URL

	_, err := client.Asset.Delete(nil, &AssetDeleteOpts{
		Name: "foo",
		Path: "bar",
	})
	require.NoError(t, err)
}
