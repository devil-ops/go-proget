package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.oit.duke.edu/devil-ops/go-proget"
)

func main() {
	if len(os.Args) != 3 {
		log.Fatalf("Usage: %v ASSETNAME PATH", os.Args[0])
	}
	client, err := proget.NewClientWithEnv(nil, nil)
	if err != nil {
		panic(err)
	}
	_, err = client.Asset.Delete(nil, &proget.AssetDeleteOpts{
		Name: os.Args[1],
		Path: os.Args[2],
	})
	if err != nil {
		panic(err)
	}
	fmt.Println("Deleted!")
}
