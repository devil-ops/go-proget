package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.oit.duke.edu/devil-ops/go-proget"
	"gopkg.in/yaml.v3"
)

func main() {
	if len(os.Args) != 3 {
		log.Fatalf("Usage: %v ASSETNAME DIR", os.Args[0])
	}
	client, err := proget.NewClientWithEnv(nil, nil)
	if err != nil {
		panic(err)
	}
	assets, _, err := client.Asset.List(nil, &proget.AssetListOpts{
		Name:      os.Args[1],
		Directory: os.Args[2],
	})
	if err != nil {
		panic(err)
	}
	b, err := yaml.Marshal(assets)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(b))
}
