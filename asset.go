package proget

import (
	"context"
	"errors"
	"fmt"
	"net/http"
)

type AssetService interface {
	List(*context.Context, *AssetListOpts) ([]Asset, *Response, error)
	Delete(*context.Context, *AssetDeleteOpts) (*Response, error)
}

type AssetServiceOp struct {
	client *Client
}

type AssetListOpts struct {
	Name      string `json:"name"`
	Directory string `json:"directory,omitempty"`
	Recursive bool   `json:"recursive,omitempty"`
}
type AssetDeleteOpts struct {
	Name      string `json:"name"`
	Path      string `json:"path,omitempty"`
	Recursive bool   `json:"recursive,omitempty"`
}

type CacheHeader struct {
	Type string `json:"type,omitempty"`
}

type Asset struct {
	Name        string      `json:"name,omitempty"`
	Parent      string      `json:"parent,omitempty"`
	Content     string      `json:"content,omitempty"`
	Created     string      `json:"created,omitempty"`
	Md5         string      `json:"md5,omitempty"`
	Modified    string      `json:"modified,omitempty"`
	Sha1        string      `json:"sha1,omitempty"`
	Sha256      string      `json:"sha256,omitempty"`
	Sha512      string      `json:"sha512,omitempty"`
	Type        string      `json:"type,omitempty"`
	Size        int64       `json:"size,omitempty"`
	CacheHeader CacheHeader `json:"cacheHeader,omitempty"`
}

func (svc *AssetServiceOp) List(ctx *context.Context, opts *AssetListOpts) ([]Asset, *Response, error) {
	path, err := pathFromAssetListOpts(*opts)
	if err != nil {
		return nil, nil, err
	}
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/%s", svc.client.BaseURL, path), nil)
	if err != nil {
		return nil, nil, err
	}
	var r []Asset
	_, err = svc.client.sendRequest(req, &r)
	if err != nil {
		return nil, nil, err
	}

	return r, nil, nil
}

func pathFromAssetListOpts(opts AssetListOpts) (string, error) {
	if opts.Name == "" {
		return "", errors.New("Must set 'Name' in opts")
	}
	s := fmt.Sprintf("endpoints/%v/dir/%v?recursive=%v", opts.Name, opts.Directory, opts.Recursive)
	return s, nil
}

func (svc *AssetServiceOp) Delete(ctx *context.Context, opts *AssetDeleteOpts) (*Response, error) {
	path, err := pathFromAssetDeleteOpts(*opts)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest("POST", fmt.Sprintf("%s/%s", svc.client.BaseURL, path), nil)
	if err != nil {
		return nil, err
	}
	var r interface{}
	_, err = svc.client.sendRequest(req, &r)
	if err != nil {
		return nil, err
	}

	return nil, nil
}

func pathFromAssetDeleteOpts(opts AssetDeleteOpts) (string, error) {
	if opts.Name == "" {
		return "", errors.New("Must set 'Name' in opts")
	}
	s := fmt.Sprintf("endpoints/%v/delete/%v?recursive=%v", opts.Name, opts.Path, opts.Recursive)
	return s, nil
}
